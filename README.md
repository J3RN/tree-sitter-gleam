# Tree Sitter Gleam

Gleam grammar for [tree-sitter](https://github.com/tree-sitter/tree-sitter)

References

- [Gleam's parser](https://github.com/gleam-lang/gleam/blob/main/src/parse.rs)

### Differences from the Gleam compiler

These can be changed if there is a need.

#### Constants

Can hold any type and value. In Gleam constants have a restricted set of types and values they can hold.

#### BitStrings

In this parser bitstring segment options can be any word and can all take an argument. In Gleam
bitstring segment options are limited to certain words and only size and unit can take an argument.

#### TypeConstructor Arguments

Named and anonymous arguments can be specified in any order. In Gleam anonymous arguments must come before named arguments.

#### External Functions

Can take named, anonymous, or labelled parameters. In Gleam they can only take named or anonymous.

#### Functions

Don't need anything in their block. In gleam their block must have at least on expression.

#### Case Statement Guards

Can have any operator. In Gleam only certain operators can be in case guards.
